filetype off
filetype plugin indent on

set tabstop=8
set autoindent
set copyindent
set encoding=utf-8
set nobackup
set nocompatible
set noerrorbells
set noswapfile
set nowrap
set number
set showmode
set smarttab
set termencoding=utf-8
set visualbell
set wildmenu
set wildmode=list:longest
set cursorline
set relativenumber

syntax on

"set termguicolors
"set background=dark
"colorscheme kuroi

set path+=**
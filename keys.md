# Keys

## GPG
~~~.bash
gpg --full-gen-key --expert
gpg --list-keys --keyid-format LONG
gpg --armor --export <key id>
gpg --delete-secret-keys <key id>
gpg -a --gen-revoke <email>
gpg --expert --edit-key <email>
gpg -K
gpg -k
~~~

### Encrypting
~~~.bash
gpg -r USER-ID -e <arquivo>
~~~

### Decrypting
~~~.bash
gpg -d <arquivo>
~~~

### Signing
~~~.bash
gpg --output doc.sig --sign doc
gpg --output doc.sig --detach-sig doc
~~~

### Verifying
~~~.bash
gpg --verify doc
~~~

## SSH

~~~.bash
ssh-keygen -t rsa -b 8192 -E SHA512 -C "github@email.com" -f <file.key> -o -a 1000
eval "$(ssh-agent -s)"
ssh-add <path to .key>
~~~

## OpenSSL

### Key gen
~~~.bash
openssl genrsa -des3 -out arquivo.key 8192
chmod 400 arquivo.key
ssh-keygen -y -f arquivo.key > arquivo.pubssh
~~~

### Key check
~~~.bash
echo | openssl s_client -servername NAME -connect HOST:443 2>/dev/null | openssl X509 -noout -dates
~~~

### Certificate creation
~~~.bash
openssl req -new -key arquivo.key -out arquivo.csr
openssl x509 -signkey arquivo.key -in arquivo.csr -req -days <days> -out arquivo.crt
openssl x509 -text -noout -in arquivo.crt
~~~

### Cerificate fetch
~~~.bash
mkdir ~/.cert
openssl s_client -connect some.imap.server:port -showcerts 2>&1 < /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' | sed -ne '1,/-END CERTIFICATE-/p' > ~/.cert/some.imap.server.pem
~~~

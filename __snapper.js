const snapper = (() => {
	var steps = 5;
	var delay = 24;
	var ticking = false;
	var easing = false;
	var doEasing = true;
	const easeFunction = (t) => t*(2-t);
	const anchors = [];
	var currentPane = 0;

	const listeners = [];

	const calculateDistance = (y, el) => { return Math.abs(y - el.top); }

	const add = (e) => { anchors.push({element: e, top: e.offsetTop, id: e.id}); }

	const addListener = (f) => { listeners.push(f); }

	const fireScroll = (endY, values) => {
		//console.log('firing ease', values);
		if (values.length > 0) {
			const v = values.shift();

			setTimeout(() => {
				window.scrollTo(0, v);
				fireScroll(endY, values);
			}, delay);
		} else {
			console.log(window.scrollY, endY);
			while (window.scrollY != endY) {
				window.scrollTo(0, endY);
			}

			console.log('easing off');
			console.log('currentPane', currentPane);
			easing = false;

			_.each(listeners, (l) => {
				l('easeFinished', anchors[currentPane]);
			});
		}
	}

	const findClosest = (y, _anchors) => {
		var result = _anchors[0];

		_.each(_anchors, (item) => {
			if (calculateDistance(y, item) < calculateDistance(y, result)) {
				result = item;
			}
		});

		return result;
	}

	const _update = (window, currentY) => {
		const closest = findClosest(currentY, anchors);
		const paneIndex = anchors.indexOf(closest);

		if (paneIndex != currentPane) {
			currentPane = paneIndex;

			const endY = anchors[currentPane].top;
			if (doEasing) {
				const distance = endY - currentY;
				const stepValue = 1/steps;
				var values = [];

				for(var i = 0; i < steps; i++) {
					const t = stepValue * i;
					values[i] = currentY + (distance * easeFunction(t));
				}

				values.push(currentY + distance);
				/*
				console.log('Current Y', currentY);
				console.log('endY', endY);
				console.log('distance', distance);
				console.log('steps', steps);
				console.log('stepValue', stepValue);
				console.log('values', values);
				*/

				if (!easing) {
					console.log('easing on');
					easing = true;
					fireScroll(endY, values);
				}
			} else {
				console.log(window.scrollTo(0, endY));
			}
		}
	}

	const update = (window) => {
		const currentY = window.scrollY;

		if (!ticking) {
			window.requestAnimationFrame(() => {
				_update(window, currentY);
				ticking = false;
			});
		}

		ticking = true;
	}

	return {
		add,
		update,
		addListener
	}
})();

#!/bin/bash
IMAGE=mysql:latest
ROOT_PASSWORD=123456

docker run --name db-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=$ROOT_PASSWORD --rm $IMAGE

#!/bin/bash
PASSWORD=123456
USER=root
SQL_DIR=$PWD
IMAGE=mysql:latest

IP=$(ip addr show wlp1s0 | grep 'inet ' | awk '{print $2}' | cut -f1 -d'/')
IP=192.168.1.56
echo Using ip: $IP
echo SQL Dir: $SQL_DIR
CMD="exec mysql -h $IP -P3306 -u $USER -p'$PASSWORD'"
docker run -it --link db-mysql --name db-mysql-client -v $SQL_DIR:/data --rm $IMAGE sh -c "$CMD"

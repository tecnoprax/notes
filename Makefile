BIN=bin
SRC=$(wildcard *.md)

PDF=$(SRC:.md=.pdf)
HTML=$(SRC:.md=.html)

all: html

init:
	mkdir -p $(BIN)

%.pdf: %.md
	pandoc -s -f commonmark -o $@ $<
	mv $@ $(BIN)

%.html: %.md
	pandoc -s -f commonmark -o $@ $<
	mv $@ $(BIN)

pdf: init $(PDF)

html: init $(HTML)

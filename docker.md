# Docker scripts/commands

## MySQL

### Start server
~~~.bash
#!/bin/bash
docker run --name db1 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 --rm mysql:latest
~~~

### Connect
~~~.bash
#!/bin/bash
IP=$(ip addr show wlp1s0 | grep 'inet ' | awk '{print $2}' | cut -f1 -d'/')
echo Using ip: $IP
CMD="exec mysql -h$IP -P3306 -uroot -p'123456'"
docker run -it --link db1 -v $PWD:/data --rm mysql:latest sh -c "$CMD"
~~~

## Portainer

### Run
~~~.bash
#!/bin/bash
docker volume create portainer_data
docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
~~~

## Tomcat 

### Run
~~~.bash
#!/bin/bash
docker run -it --rm -p 8888:8080 tomcat:8.0
~~~
